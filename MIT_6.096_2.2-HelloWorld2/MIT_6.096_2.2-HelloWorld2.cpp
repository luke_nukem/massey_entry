#include <iostream>

using namespace std;

int main()
{
    int input;

    cout << "Please enter a positive number, to run each loop: ";
    cin >> input;

    for (int i = 0; i < input; ++i)
    {
    cout << "For loop #" << i << endl;
    }

    int w = 0;
    while (w < input)
    {
        cout << "While loop #" << w << endl;
        ++w;
    }

    int d = 0;
    do
    {
        cout << "Do loop #" << d << endl;
        ++d;
    }
    while (d < input);

    return 0;
}

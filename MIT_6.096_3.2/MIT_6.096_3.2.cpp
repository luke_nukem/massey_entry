#include <iostream>

using namespace std;

int main()
{
    int inAmount = 0;
    double total = 0.0;
    double maxNum = 0;

    cout << "How many numbers would you like?" << endl;
    cin >> inAmount;
    int nList[inAmount];
    int minNum = inAmount;

    for (int i =0; i < inAmount; i++)
    {
        cout << "Enter number for slot " << i << ": ";
        cin >> nList[i];
        total += nList[i];
    }

    for (int i = 0; i < inAmount; i++)
    {
        //if (nList[i] < minNum)
        //    minNum = nList[i];
        minNum = nList[i] < minNum ? nList[i] : minNum;
        //if (nList[i] > maxNum)
        //    maxNum = nList[i];
        maxNum = nList[i] > maxNum ? nList[i] : maxNum;
    }

    cout << "Total = " << total << endl;
    cout << "Mean = " << (total/inAmount) << endl;
    cout << "Max = " << maxNum << endl;
    cout << "Min = " << minNum << endl;
    cout << "Range = " << (maxNum-minNum);

    return 0;
}

/*
    Heck of a round-about way to do this.
    Exercising different skills really, as well as practicing different aspects.
    The whole program could be trimmed down, and then more if I take out the ability
    to enter numbers to calculate prime for.

    Tempted to use K&R bracing. Seems to be used a lot in JAVA too.
    Okay, K&R for now.
*/

#include <iostream>

using namespace std;

bool enterOwn = false;

int getNum(){
    char answer;
    int i = 0;
    cout << "How many numbers would you like to calculate prime for?\n";
    cout << "Number must be greater than 3\n";
    while(true){
        cin >> i;
        while(true){
            if (i <= 3){
                cout << "A '3' or less was entered, please try again\n";
                break; // Breaks back to CIN >> I; to re-get number
            }
            // only gets to this point if the fi statement doesn't catch a 3 or less
            cout << "Would you like to enter your own numbers? y/n \n";
            cin >> answer;
            switch (answer){
                case 'y':
                    enterOwn = true;
                    break;
                case 'n':
                    enterOwn = false;
                    break;
                default:
                    enterOwn = false;
                    break;
            }
            return i;
        }
    }
}

// Trying K&R bracing
int enterNum(int i){
    while (true){
        int n = 0;
        cout << "Please enter any number greater than 1 " << endl;
        cin >> n;
        while (true){
            if (n <= 1){
                cout << "A '1' or less was entered, please try again\n";
                break;
            }
            if (n >= 2)
                return n;
        }
    }
}

int calcPrime (int amount, int num){
    for (int i=2; i<amount; i++){
        bool prime=true;
        for (int j=2; j*j<=i; j++){
            if (i % j == 0){
                if ((i==num)&&(enterOwn==true)){
                    prime=false;
                    cout << num << " is not a prime" << endl;
                    return 1;
                }
                else if (enterOwn==false){
                    prime=false;
                    break;
                }
            }
        }
        if(prime){
            if ((i == num)&&(enterOwn==true)){
                cout << num << " is a prime" << endl;
                return 1;
            }
            else if (enterOwn==false)
                cout << i << endl;
        }
    }
    return 0; // Need return statement to prevent the function from outputting all primes up to num
}

int main(){
    int s = getNum();
    for (int i = 0; i < s; i++)
        if (enterOwn == true){
            int nList[s];
            nList[i] = enterNum(i);
            calcPrime(nList[s], nList[i]);
        }
        else{
            calcPrime(s,0);
            i = s;
        }
    return 0;
}

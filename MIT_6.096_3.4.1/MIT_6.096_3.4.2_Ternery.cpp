#include <iostream>

using namespace std;

int main()
{
    while (true){
        cout << "Please input a number to be divided by 5\n";
        int in = 0;
        cin >> in;
        int yaynay = (in % 5) ? 1 : in;

        if (yaynay != 1)
            cout << in << " is divisible by 5\n";
        else if ((yaynay == 1)&&(in == -1)){
            cout << "Program exited";
            break;
        }
        else
            cout << in << " is not divisible by 5\n";
    }
    return 0;
}

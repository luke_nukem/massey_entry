#include <iostream>

using namespace std;

int main(int argc, char** argv) {

    for (int i = 0; i < 3; i++) // Run the block 3 times
    {
        int w = 0;
        while (true) // Set it to run until break
        {
            if (w >= 3) // Breaks out of loop to outer loop,
                break;  // which will then run this again
                        // Also "w > 3" runs 4 times, "w >= 3" runs 3 times.
            //else --Isn't needed, there is no {} block to execute, only a statement
            cout << "Hello World in a \"for\\while\" loop" << "\n";
            w++; // Doesn't matter if prefix or postfix here.
        }
    } // Three FOR times three WHILE = 9
    return 0;
}
